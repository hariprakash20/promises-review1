const getUsers = () => fetch("https://jsonplaceholder.typicode.com/users");

const getTasks = () => fetch("https://jsonplaceholder.typicode.com/todos");

function get5Tasks(users, tasks){
    let userTasks = {};
    for(let i=0; i<5;i++){
        userTasks[users[i].name+"_"+users[i].id] =[]
        tasks.map(task=>{
            if(task.userId == users[i].id && userTasks[users[i].name+"_"+users[i].id].length < 5){
                userTasks[users[i].name+"_"+users[i].id].push(task);
            }
        })
    }
    console.log(userTasks);
    printTasks(userTasks)
}

const renderList = (id, completed, title) => {
    return `<div class="task">
    <input type="checkbox" id="${id}" ${completed? 'checked="checked"' : ''}">
    <p class="title"> ${title}</p>
</div>`
}

function printTasks(userTasks){
    Object.keys(userTasks).map(user=> {
        let userName = user.split('_')[0];
        userHTML = `<h1>${userName}</h1>`;
        mainContainer.innerHTML += userHTML;
        userTasks[user].map( task => {
            let taskHTML = renderList(task.id, task.completed, task.title);
            mainContainer.innerHTML += taskHTML;
        })
    });
}


